package com.tokioschool.myshop.repository.user;

import com.tokioschool.myshop.domain.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserDao extends JpaRepository<User,String > {

    Page<User> findAllByPerson_UserNameContainingIgnoreCase(String filerByName, PageRequest pageRequest);

    @Query("SELECT COUNT(1) FROM User")
    int getSizeUsers();

    @Query("SELECT COUNT(1) FROM User us join Person pr on us.person.id = pr.id WHERE pr.userName LIKE (%:name%)")
    int getSizeUsersByName(String name);
}
