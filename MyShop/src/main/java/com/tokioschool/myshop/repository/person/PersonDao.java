package com.tokioschool.myshop.repository.person;

import com.tokioschool.myshop.domain.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonDao extends JpaRepository<Person,String > {

    Optional<Person> findPersonById(String id);
}
