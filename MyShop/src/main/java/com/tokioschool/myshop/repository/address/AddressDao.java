package com.tokioschool.myshop.repository.address;

import com.tokioschool.myshop.domain.address.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AddressDao extends JpaRepository<Address,Long> {

    Optional<Address> findAddressById(Long id);
}
