package com.tokioschool.myshop.repository.product;

import com.tokioschool.myshop.domain.product.Product;
import com.tokioschool.myshop.proyection.product.ProductCounterByCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductDao extends JpaRepository<Product,Long> {


    List<Product> findProductsByCategoryContains(String category);

    @Query("SELECT p.category as category, COUNT(1) as counter FROM Product p WHERE p.category LIKE (%:category%)")
    Optional<ProductCounterByCategory> getProductCountByCategory(@Param("category") String category);

    @Query("SELECT p.category as category, COUNT(1) as counter FROM Product p GROUP BY p.category")
    List<ProductCounterByCategory> getProductCountByCategories();
    List<Product> findProductsByNameContains(String name);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Product p SET p.stock = :stock WHERE p.id = :id")
    int updateProductStock(@Param("id") Long productId, @Param("stock")BigDecimal stock);


    @Query("SELECT COUNT(1) as numberProducts FROM Product p WHERE p.visibly = true")
    int getSizeProductsVisibly();

    @Query("SELECT COUNT(1) as numberProducts FROM Product p WHERE p.visibly = true and p.name LIKE (%:name%)")
    int getSizeProductsVisiblyByNameContains(@Param("name") String name);

    Page<Product> findProductsByNameContainsIgnoreCaseAndVisiblyIsTrue(String keyword, PageRequest pageRequest);

    List<Product> findProductByNameContainsIgnoreCaseAndVisiblyIsTrue(String keyword);
    List<Product> findProductByVisiblyIsTrue();

    Page<Product> findAllByVisiblyIsTrue(PageRequest pageRequest);

    Page<Product> findAllByNameContainingIgnoreCaseAndVisiblyIsTrue(String filterByName, PageRequest pageRequest);

    Page<Product>  findAllByName(String name, PageRequest any);

    @Query(
            value = """
                UPDATE Product p 
                SET p.visibly = :visibly 
                WHERE p.id = :id 
            """)
    @Modifying(clearAutomatically = true)
    int updateVisiblyProduct(@Param("id") Long flightId,@Param("visibly") boolean visibly);
}
