package com.tokioschool.myshop.dto.identification;

public enum IdentificationTypeDto {
    NIF,CIF,PASSPORT,SID,OTRO
}
