package com.tokioschool.myshop.dto.user;

import com.tokioschool.myshop.dto.addres.AddressDto;
import com.tokioschool.myshop.dto.person.PersonDto;
import com.tokioschool.myshop.dto.resource.ResourceDto;
import com.tokioschool.myshop.util.validation.identification.PersonIdentificationValidation;
import com.tokioschool.myshop.util.validation.password.PasswordValidation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Builder
@Data
@NoArgsConstructor @AllArgsConstructor
@PasswordValidation
public class UserDto{

    private String id;
    @Valid
    //@PersonIdentificationValidation
    private PersonDto person;

    /*@NotEmpty(message = "The password can not be empty")
    @NotNull(message = "The password can not be null")
    @Size(min = 8, max = 20, message = "La contraseña debe tener entre 8 y 20 caracteres")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{8,20}$",
            message = "La contraseña debe contener al menos una letra mayúscula, una letra minúscula, un dígito, un carácter especial y no puede contener espacios en blanco")
    */
    private String newPassword;
    private String password;
    @Valid
    private AddressDto address;

    private ResourceDto resource;

    private Boolean active;

}
