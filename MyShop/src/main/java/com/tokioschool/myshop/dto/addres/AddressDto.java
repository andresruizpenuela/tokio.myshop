package com.tokioschool.myshop.dto.addres;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressDto {

    private Long id;

    @NotNull(message = "The address can not be null")
    private RoadTypeDto roadType;

    @NotEmpty(message = "The road can not be empty")
    @NotNull(message = "The road can not be null")
    private String road;

    @Positive
    @NotNull(message = "The number can not be null")
    private Integer number;

    private String rest;

    @Positive
    @NotNull(message = "The postalCode can not be null")
    @Digits(integer = 5, fraction = 0, message = "El valor debe ser un número entero sin decimales de 5 digitos")
    private Integer postalCode;

    @NotEmpty(message = "The localitation can not be empty")
    @NotNull(message = "The localitation can not be null")
    private String localitation;

    @NotEmpty(message = "The province can not be empty")
    @NotNull(message = "The province can not be null")
    private String province;

    @NotEmpty(message = "The country can not be empty")
    @NotNull(message = "The country can not be null")
    private String country;
}
