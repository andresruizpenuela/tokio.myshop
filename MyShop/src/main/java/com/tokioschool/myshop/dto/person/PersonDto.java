package com.tokioschool.myshop.dto.person;

import com.tokioschool.myshop.dto.identification.IdentificationDto;
import com.tokioschool.myshop.util.helper.EmailHelper;
import com.tokioschool.myshop.util.validation.email.EmailValidation;
import com.tokioschool.myshop.util.validation.identification.IdentificationValidation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
public class PersonDto {

    private String id;

    @NotEmpty(message = "The userName can not be empty")
    @NotNull(message = "The userName can not be null")
    private String userName;

    private String surnames;

    @NotEmpty(message = "The email can not be empty")
    @NotNull(message = "The email can not be null")
    //@Email(message = "The email haven't format validate")
    //@EmailValidation
    @Pattern(regexp = EmailHelper.regex,message = "The email haven't format validate")
    private String email;

    @Valid
    @IdentificationValidation
    private IdentificationDto identification;
}
