package com.tokioschool.myshop.dto.product;

import com.tokioschool.myshop.dto.resource.ResourceDto;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
public class ProductDto {

    private Long id;

    @NotEmpty(message = "Product name cannot be empty.")
    @NotBlank(message = "Product name cannot be blank.")
    @Size(min = 3,max = 20,message = "Product name should is 3 to 20 digits")
    private String name;

    private String description;

    @NotNull(message = "Category of Product cannot be null.")
    @NotEmpty(message = "Category of Product cannot be empty.")
    private String category;
    private Boolean visibly;

    @NotNull(message = "Stock of Product cannot be null.")
    @Min(value=0)
    private BigDecimal stock;

    @NotNull(message = "Price of Product cannot be null.")
    @Min(value=0)
    private BigDecimal price;

    @NotNull(message = "Discount of Product cannot be null.")
    @Min(value=0)
    private BigDecimal discount;

    @NotNull(message = "Category of Product cannot be null.")
    @Min(value=0)
    private BigDecimal taxes;
    private ResourceDto image;

}
