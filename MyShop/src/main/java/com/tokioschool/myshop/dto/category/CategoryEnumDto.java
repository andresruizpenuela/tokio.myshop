package com.tokioschool.myshop.dto.category;

public enum CategoryEnumDto {
    ELECTRONICA, TABLET, NOTEBOOK, WORKSTATION, SMARTPHONE
}
