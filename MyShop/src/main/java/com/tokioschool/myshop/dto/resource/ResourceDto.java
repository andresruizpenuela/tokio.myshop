package com.tokioschool.myshop.dto.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor @NoArgsConstructor
@Builder
public class ResourceDto {

    private UUID id;
    private String name;
    private String contentType;
    private Integer size;

}
