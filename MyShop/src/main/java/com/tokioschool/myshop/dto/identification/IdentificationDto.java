package com.tokioschool.myshop.dto.identification;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor @NoArgsConstructor
public class IdentificationDto {

    private Long id;

    @NotEmpty(message = "The number can not be empty")
    @NotNull(message = "The number can not be null")
    private String number;

    @NotNull(message = "The identificationType can not be null")
    private IdentificationTypeDto identificationType;

}
