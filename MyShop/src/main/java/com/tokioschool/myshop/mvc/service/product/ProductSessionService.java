package com.tokioschool.myshop.mvc.service.product;

import com.tokioschool.myshop.dto.product.ProductDto;
import com.tokioschool.myshop.mvc.dto.product.CartItemDto;
import com.tokioschool.myshop.mvc.dto.product.ProductCarDto;
import com.tokioschool.myshop.mvc.dto.product.ProductSessionDto;
import org.springframework.lang.NonNull;

import java.util.List;

public interface ProductSessionService {

    List<ProductCarDto> getListProductCar(@NonNull final ProductSessionDto productSessionDto);

    boolean addProductToCar(Long productId, ProductSessionDto productSessionDto);

    void updateQuantity(ProductSessionDto productSessionDto, CartItemDto cartItem);
}
