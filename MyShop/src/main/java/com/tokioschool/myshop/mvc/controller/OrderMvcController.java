package com.tokioschool.myshop.mvc.controller;

import com.tokioschool.myshop.mvc.dto.product.ProductSessionDto;
import com.tokioschool.myshop.mvc.service.product.ProductSessionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping("/shopping/orders")
@RequiredArgsConstructor
@SessionAttributes(names="productSessionDto")
public class OrderMvcController {

    private final ProductSessionService productSessionService;

    @ModelAttribute(name="productSessionDto")
    public ProductSessionDto productSessionDto(){ return new ProductSessionDto();}

    @GetMapping({"","/","/new-order"})
    public String newOrderHandler(@ModelAttribute("productSessionDto") ProductSessionDto productSessionDto, SessionStatus sessionStatus){
        // limpia la que el ESTE contralador esta gestionando en sesion
        sessionStatus.setComplete();

        return "oder/order-form";
    }
}
