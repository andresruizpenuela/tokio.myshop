package com.tokioschool.myshop.mvc.service.product.imp;

import com.tokioschool.myshop.dto.product.ProductDto;
import com.tokioschool.myshop.mvc.dto.product.CartItemDto;
import com.tokioschool.myshop.mvc.dto.product.ProductCarDto;
import com.tokioschool.myshop.mvc.dto.product.ProductSessionDto;
import com.tokioschool.myshop.mvc.service.product.ProductSessionService;
import com.tokioschool.myshop.service.product.IProductService;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductSessionServiceImpl implements ProductSessionService {

    private final ModelMapper modelMapper;
    private final IProductService productService;
    /**
     * This method return the list product there is inside to the shopping car
     *
     * This use the method {@link List#copyOf(Collection)} } for copy a Collection in the List
     *
     * @param productSessionDto
     * @return
     */
    @Override
    public List<ProductCarDto> getListProductCar(@NotNull final  ProductSessionDto productSessionDto) {
        return List.copyOf(productSessionDto.getMapCarProducts().values());
    }

    /**
     * This method add a product to shopping car
     *
     * This use the method {@link java.util.Map#putIfAbsent(Object, Object)}, that return null if don't exits then put element,
     * or the element if exits the element and don't put
     *
     * @param productId
     * @param productSessionDto
     * @return true if add the prodcut, otherwere fasel
     */
    @Override
    public boolean addProductToCar(Long productId, ProductSessionDto productSessionDto ) {
        final ProductDto productDto = productService.getProductById(productId);
        final ProductCarDto productCarDto = modelMapper.map(productDto,ProductCarDto.class);
        // puatIfABsent, get null if don't exist the  and put element, and return element, if exits in the map
        return Objects.isNull(productSessionDto.getMapCarProducts().putIfAbsent(productCarDto.getId(),productCarDto));
    }

    /**
     * @param productSessionDto
     * @param cartItem
     */
    @Override
    public void updateQuantity(ProductSessionDto productSessionDto, CartItemDto cartItem) {

        productSessionDto.getMapCarProducts().get(cartItem.getProductId()).setAmount(new BigDecimal(cartItem.getQuantity()));

    }
}
