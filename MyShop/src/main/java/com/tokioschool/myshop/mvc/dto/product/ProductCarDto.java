package com.tokioschool.myshop.mvc.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
public class ProductCarDto {

    private Long id;
    private String name;
    private BigDecimal stock;
    @Builder.Default
    private BigDecimal amount = new BigDecimal(0);
}
