package com.tokioschool.myshop.mvc.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
public class CartItemDto {
    private Long productId;
    private int quantity;

}
