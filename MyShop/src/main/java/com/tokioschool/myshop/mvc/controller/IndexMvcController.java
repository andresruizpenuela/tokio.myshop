package com.tokioschool.myshop.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/shopping")
public class IndexMvcController {

    @GetMapping(value= {"","/"})
    public String getIndexView(){
        return "index";
    }
}
