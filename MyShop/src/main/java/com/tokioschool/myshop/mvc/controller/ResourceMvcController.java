package com.tokioschool.myshop.mvc.controller;

import com.tokioschool.myshop.store.dto.ResourceContentDto;
import com.tokioschool.myshop.store.service.ResourceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Objects;
import java.util.UUID;

@Controller
@RequestMapping("/shopping/resources")
@RequiredArgsConstructor
@Slf4j
public class ResourceMvcController {

    private final ResourceService resourceService;

    @GetMapping(value = {"","/"})
    public ResponseEntity<byte[]> getResource(@RequestParam("rsc") UUID resourceId){

        final ResourceContentDto  resourceContentDto = resourceService.findResource(resourceId).orElse(null);

        if(Objects.isNull(resourceContentDto)){
            log.error("Resource download not found, id is invalidate");
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(resourceContentDto.contentType()))
                .contentLength(resourceContentDto.size())
                .body(resourceContentDto.content());
    }

    @GetMapping("/download/{id}")
    public ResponseEntity<byte[]> download(@PathVariable("id") UUID resourceId){

        final ResourceContentDto  resourceContentDto = resourceService.findResource(resourceId).orElse(null);

        if(Objects.isNull(resourceContentDto)){
            log.error("Resource download not found, id is invalidate");
            return ResponseEntity.notFound().build();
        }

        // wrapper para devoler objetos que Spring Serializa y los envía
        final HttpHeaders httpHeaders = new HttpHeaders();
        // fuerza la descarga del cotenido y le ponemos el nombre del archivo
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename= "+resourceContentDto.resourceName());
        // infromación del documenot en la cabecera (puede ponerse en el cuerpo)
        httpHeaders.add(HttpHeaders.CONTENT_LENGTH,String.valueOf(resourceContentDto.size()));
        httpHeaders.add(HttpHeaders.CONTENT_TYPE,resourceContentDto.contentType());

        return ResponseEntity.ok()
                .headers(httpHeaders)
                .body(resourceContentDto.content());
    }

}
