package com.tokioschool.myshop.mvc.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
public class UserMvcDto {
    private String id;
    private String personUserName;
    private String personUserSurnames;

}
