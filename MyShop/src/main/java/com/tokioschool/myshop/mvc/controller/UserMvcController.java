package com.tokioschool.myshop.mvc.controller;

import com.tokioschool.myshop.dto.user.UserDto;
import com.tokioschool.myshop.mvc.dto.UserMvcDto;
import com.tokioschool.myshop.service.user.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@Controller
@RequestMapping("/shopping/users")
@RequiredArgsConstructor
public class UserMvcController {

    private final UserService userService;

    @GetMapping(value={"","/","/list"})
    public String getListUsers(Model model,
                                     @RequestParam(value="keyword",required = false) String keyword,
                                     @RequestParam(value = "page",defaultValue = "1") int page,
                                     @RequestParam(value = "size",defaultValue = "2") int size){

        final Page<UserMvcDto> pageUserMvcDto = userService.recoverPageUserFilterName(page,size,keyword);

        model.addAttribute("pageTitle","Usuarios disponibles");
        model.addAttribute("users", pageUserMvcDto.getContent());

        model.addAttribute("currentPage", pageUserMvcDto.getNumber() + 1);
        model.addAttribute("totalItems", pageUserMvcDto.getTotalElements());
        model.addAttribute("totalPages", pageUserMvcDto.getTotalPages());
        model.addAttribute("pageSize", size);

        return "person/user-list.html";
    }
    @GetMapping(value = {"/new"})
    public String getFormNewAccount(Model model){
        model.addAttribute("title","New User");
        model.addAttribute("user", UserDto.builder().build());
        return "person/user-detail";
    }

    @GetMapping(value = {"/update/{id}"})
    public String getFormUpdateAccount(@PathVariable("id") String userId, Model model){
        model.addAttribute("title","Update User");
        model.addAttribute("user", userService.findUserById(userId));
        return "person/user-detail";
    }
    @PostMapping(value = {"/add"}, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String  postCreateOrUpdateUser(
            @RequestPart("personPhoto") MultipartFile personPhoto,
            @Valid @ModelAttribute("user") UserDto userDto, BindingResult bindingResult,
            Model model){

        if(bindingResult.hasErrors()) {
            final String pwdError = bindingResult.getAllErrors().stream().filter(objectError -> Objects.nonNull(objectError.getCode()) && objectError.getCode().contains("PasswordValidation")).findFirst()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .orElse(null);
            model.addAttribute("pwdError", pwdError);
            return "person/user-detail";
        }

        if(Objects.isNull(userDto.getId()) || userDto.getId().isBlank()) {
            userDto = userService.createUser(userDto, personPhoto);
        }else{
            userDto = userService.updateUser(userDto, personPhoto);
        }

        return  "redirect:/shopping/users/update/%s".formatted(userDto.getId());
    }


}
