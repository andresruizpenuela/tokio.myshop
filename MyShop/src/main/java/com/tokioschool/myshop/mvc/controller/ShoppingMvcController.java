package com.tokioschool.myshop.mvc.controller;

import com.tokioschool.myshop.mvc.dto.product.CartItemDto;
import com.tokioschool.myshop.mvc.dto.product.ProductSessionDto;
import com.tokioschool.myshop.mvc.service.product.ProductSessionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/shopping/shops")
@RequiredArgsConstructor
@SessionAttributes(names="productSessionDto")
public class ShoppingMvcController {

    private final ProductSessionService productSessionService;


    @ModelAttribute(name="productSessionDto")
    public ProductSessionDto productSessionDto(){ return new ProductSessionDto();}
    @GetMapping({"","/","/car"})
    public String getListShopping(final Model model,
                                  @ModelAttribute("productSessionDto") ProductSessionDto productSessionDto){
        model.addAttribute("products",productSessionService.getListProductCar(productSessionDto));
        return "shopping/shopping-list";
    }

    @GetMapping("/add/{id}/{page}/{size}")
    public RedirectView addProduct(@PathVariable("id") Long productId,
                                   @ModelAttribute("productSessionDto") ProductSessionDto productSessionDto,
                                   @PathVariable(value = "page") int page,
                                   @PathVariable(value = "size") int size,
                                   RedirectAttributes redirectAttributes){

        final boolean result = productSessionService.addProductToCar(productId,productSessionDto);
        if(result){
            redirectAttributes.addFlashAttribute("add","Product add to car");
        }else{
            redirectAttributes.addFlashAttribute("add","Product don't add");
        }
        return new RedirectView("/shopping/products?page="+page+"&size="+size);
    }

    @PostMapping("/buy-products")
    public String buyProduct(Model model){
        return "shopping/shopping-list";
    }

    @PostMapping("/updateQuantity")
    public void updateQuantity(@RequestBody CartItemDto cartItem,
                               @ModelAttribute("productSessionDto") ProductSessionDto productSessionDto) {
        productSessionService.updateQuantity(productSessionDto,cartItem);
    }

}
