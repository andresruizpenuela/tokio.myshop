package com.tokioschool.myshop.mvc.controller;

import com.tokioschool.myshop.dto.product.ProductDto;
import com.tokioschool.myshop.service.product.IProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@Controller
@RequestMapping("/shopping/products")
@RequiredArgsConstructor
public class ProductMvcController {

    private final IProductService productService;

    // Example request: http://localhost:8080/shopping/products?page=1&size=1

    @GetMapping(value= {"","/"})
    public String getAllProducts(Model model,
                                 @RequestParam(value="keyword",required = false) String keyword,
                                 @RequestParam(value = "page",defaultValue = "1") int page,
                                 @RequestParam(value = "size",defaultValue = "2") int size){

        final Page<ProductDto> productDtoPage = productService.findProductPaginateByName(keyword,page,size);

        model.addAttribute("pageTitle","Productos disponibles");
        model.addAttribute("products", productDtoPage.getContent());
        model.addAttribute("currentPage", productDtoPage.getNumber() + 1);
        model.addAttribute("totalItems", productDtoPage.getTotalElements());
        model.addAttribute("totalPages", productDtoPage.getTotalPages());
        model.addAttribute("pageSize", size);

        return "product/product-list";
    }

    @GetMapping(value={"/detail/{id}"})
    public String getProductDetail(@PathVariable("id") Long productId, Model model){

        final ProductDto productDto = productService.getProductById(productId);
        model.addAttribute("product",productDto);

        return "product/product-detail";

    }

    @GetMapping(value={"/edit/{id}"})
    public String getEditProduct(@PathVariable("id") Long productId, Model model){

        final ProductDto productDto = productService.getProductById(productId);
        model.addAttribute("product",productDto);
        //model.addAttribute("categories", CategoryEnumDto.values());
        return "product/product-edit";
    }

    @PostMapping(value={"/edit/","/edit/{id}"})
    public ModelAndView postCreateOrEditProduct(@PathVariable(value = "id",required = false) Long productId,
                                                @RequestParam("productImage") MultipartFile multipartFile,
                                                @Valid @ModelAttribute("product") ProductDto productDto, // Recibe el objeto del modelo contenido en la vista
                                                BindingResult result
                                  ){

        if(result.hasErrors()){
            final ModelAndView modelAndView  = new ModelAndView("product/product-edit");
            modelAndView.addObject("product",productDto);
            return modelAndView;
        }
        if(Objects.nonNull(productId))
            this.productService.editProduct(productId,productDto,multipartFile);
        else{
            this.productService.createProduct(productDto,multipartFile);
        }
        // redirect
        return new ModelAndView("redirect:/shopping/products/");
    }

    @GetMapping("/create")
    public String getCreateProduct(Model model){

        final ProductDto productDto = ProductDto.builder().build();
        model.addAttribute("product",productDto);
        //model.addAttribute("categories", CategoryEnumDto.values());
        return "product/product-edit";
    }

    @PostMapping("/create")
    public ModelAndView postCreateProduct(@RequestParam("productImage") MultipartFile multipartFile,
                                   @ModelAttribute("product") ProductDto productDto){

        this.productService.createProduct(productDto,multipartFile);
        // redirect
        return new ModelAndView("redirect:/shopping/products");
    }

    @RequestMapping(value = "/delete/{id}",method = {RequestMethod.GET,RequestMethod.DELETE})
    public String deleteProductById(@PathVariable("id") Long productId){
        this.productService.deleteProductById(productId);

        return "redirect:/shopping/products";
    }

    @RequestMapping(value="/update-visibility/{id}",method = {RequestMethod.GET,RequestMethod.PUT})
    public String updateVisibility(@PathVariable("id") Long productId){
        this.productService.updateVisiblyProduct(productId);
        return "redirect:/shopping/products/edit/%s".formatted(productId);
    }
}
