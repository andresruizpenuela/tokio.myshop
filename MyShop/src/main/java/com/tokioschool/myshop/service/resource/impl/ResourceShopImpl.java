package com.tokioschool.myshop.service.resource.impl;

import com.tokioschool.myshop.domain.resource.Resource;
import com.tokioschool.myshop.dto.resource.ResourceDto;
import com.tokioschool.myshop.service.resource.ResourceShopService;
import com.tokioschool.myshop.store.dto.ResourceContentDto;
import com.tokioschool.myshop.store.dto.ResourceIdDto;
import com.tokioschool.myshop.store.service.ResourceService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@AllArgsConstructor
public class ResourceShopImpl implements ResourceShopService {

    private final ResourceService resourceService;
    @Override
    public ResourceDto saveResource(@NonNull MultipartFile multipartFile) {

        final ResourceIdDto resourceIdDto = resourceService.saveResource(multipartFile,"shopping-web")
                .orElseThrow(()->new IllegalStateException("Resource no save in store"));

        return ResourceDto.builder()
                .contentType(multipartFile.getContentType())
                .id(resourceIdDto.resourceId())
                .name(multipartFile.getOriginalFilename())
                .size((int)multipartFile.getSize())
                .build();
    }
}
