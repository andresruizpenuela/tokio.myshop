package com.tokioschool.myshop.service.user;

import com.tokioschool.myshop.dto.user.UserDto;
import com.tokioschool.myshop.mvc.dto.UserMvcDto;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {

    UserDto createUser(@NonNull UserDto userDto, @Nullable MultipartFile multipartFile);
    UserDto updateUser(@NonNull UserDto userDto, @Nullable MultipartFile multipartFile);

    UserDto findUserById(@NonNull String userId);
    Page<UserMvcDto> recoverPageUserFilterName(int page, int pageSize,@Nullable String keyword);
}
