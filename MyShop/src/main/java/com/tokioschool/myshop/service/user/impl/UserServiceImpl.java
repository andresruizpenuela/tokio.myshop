package com.tokioschool.myshop.service.user.impl;

import com.tokioschool.myshop.domain.address.Address;
import com.tokioschool.myshop.domain.address.RoadType;
import com.tokioschool.myshop.domain.identification.Identification;
import com.tokioschool.myshop.domain.identification.IdentificationType;
import com.tokioschool.myshop.domain.person.Person;
import com.tokioschool.myshop.domain.resource.Resource;
import com.tokioschool.myshop.domain.user.User;
import com.tokioschool.myshop.dto.addres.AddressDto;
import com.tokioschool.myshop.dto.identification.IdentificationDto;
import com.tokioschool.myshop.dto.person.PersonDto;
import com.tokioschool.myshop.dto.resource.ResourceDto;
import com.tokioschool.myshop.dto.user.UserDto;
import com.tokioschool.myshop.mvc.dto.UserMvcDto;
import com.tokioschool.myshop.repository.address.AddressDao;
import com.tokioschool.myshop.repository.person.PersonDao;
import com.tokioschool.myshop.repository.user.UserDao;
import com.tokioschool.myshop.service.resource.ResourceShopService;
import com.tokioschool.myshop.service.user.UserService;
import com.tokioschool.myshop.util.helper.StringUtil;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.util.Strings;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final PersonDao personDao;
    private final AddressDao addressDao;

    private final ResourceShopService resourceShopService;

    private final ModelMapper modelMapper;

    /**
     * @param userDto
     * @param multipartFile
     * @return
     */
    @Override
    public UserDto createUser(UserDto userDto, MultipartFile multipartFile) {
        userDto = clearSpacesInFields(userDto);

        User user = User.builder().build();

        user = populationCreateOrUpdateUser(user, userDto, multipartFile);

        return modelMapper.map(user, UserDto.class);
    }

    /**
     * @param userDto
     * @param multipartFile
     * @return
     */
    @Override
    public UserDto updateUser(UserDto userDto, MultipartFile multipartFile) {
        userDto = clearSpacesInFields(userDto);

        final String userId = userDto.getId();
        User user = populationFindUserById(userId);

        user = populationCreateOrUpdateUser(user,userDto,multipartFile);
        return modelMapper.map(user, UserDto.class);
    }

    /**
     * @param userId
     * @return
     */
    @Override
    public UserDto findUserById(String userId) {
        final  User user = populationFindUserById(userId);

        return modelMapper.map(user, UserDto.class);
    }

    /**
     * @param page
     * @param size
     * @param keyword
     * @return
     */
    @Override
    public Page<UserMvcDto> recoverPageUserFilterName(int page, int pageSize, String keyword) {

        final String filerName = Optional.ofNullable(keyword)
                .map(StringUtils::stripToEmpty)
                .orElse(Strings.EMPTY);

        // build page request
        final int numberUserAvailable;
        if(filerName.isBlank()){
            numberUserAvailable = getNumberUserAvailable();
        }else{
            numberUserAvailable = getNumberUserAvailableFilterName(keyword);
        }

        if(numberUserAvailable == NumberUtils.INTEGER_ZERO){ // dont' user in the system
            return Page.empty();
        }

        final int numberPageRequest = validatePage(page,pageSize,numberUserAvailable);
        final int numberElementsForPage = validatePageSize(pageSize,numberUserAvailable);
        final PageRequest pageRequest = PageRequest.of(numberPageRequest,numberElementsForPage, Sort.by(Sort.Direction.DESC,"person.userName"));

        // find user and recover a page
        final Page<User> userPage;
        if(filerName.isBlank()){
            userPage = userDao.findAll(pageRequest);
        }else{
            userPage = userDao.findAllByPerson_UserNameContainingIgnoreCase(filerName,pageRequest);
        }

        // map
        final List<UserMvcDto> userMvdDtoList = userPage.getContent().stream().map(user -> modelMapper.map(user, UserMvcDto.class)).toList();
        return new PageImpl<>(userMvdDtoList, pageRequest, numberUserAvailable);
    }

    private int getNumberUserAvailable(){
        return userDao.getSizeUsers();
    }
    private int getNumberUserAvailableFilterName(@NonNull final String filterName){
        return userDao.getSizeUsersByName(filterName);
    }

    private int numberPageAllow(int elementsForPage, int totalElements){
        return (int) Math.ceil(totalElements/(double)elementsForPage);
    }
    private int validatePage(int pageRequest, int usersForPage, int totalUsers) {
        // Page go 0 to ( Match.cell(totalProducts/(double)productsForPage) -1 )
        final int numberPageAllow = this.numberPageAllow(usersForPage,totalUsers);
        if(pageRequest <= BigInteger.ZERO.intValue() ){
            // return last page
            return numberPageAllow - BigInteger.ONE.intValue();
        }
        if(pageRequest > numberPageAllow){
            // return first page
            return BigInteger.ZERO.intValue();
        }
        // return page request
        return  pageRequest - BigInteger.ONE.intValue();
    }
    private int validatePageSize(int userForPage,int totalProducts) {
        if( userForPage<=0 || userForPage >= totalProducts){
            return totalProducts;
        }
        return userForPage;
    }
    /**
     * Methdo that find an account given id
     * @param userId identification of user
     * @return instance of user
     * @throws IllegalStateException when don't find user
     */
    private User populationFindUserById(String userId) throws IllegalArgumentException{
        return userDao.findById(userId)
                .orElseThrow(
                        ()-> new IllegalArgumentException("Error, user with id %s not found".formatted(userId)));
    }
    @Transactional
    protected User populationCreateOrUpdateUser(User user, UserDto userDto, MultipartFile multipartFile){

        Resource userImg = Optional.ofNullable(user.getResource()).orElse(null);
        if(Objects.nonNull(multipartFile) && !multipartFile.isEmpty()){
            final ResourceDto resourceDto = resourceShopService.saveResource(multipartFile);
            userImg = modelMapper.map(resourceDto,Resource.class);
        }

        // map object


        Person person = Optional.ofNullable(user.getPerson()).orElse(Person.builder().build());
        person.setUserName(userDto.getPerson().getUserName());
        person.setSurnames(userDto.getPerson().getSurnames());
        person.setEmail(userDto.getPerson().getEmail());

        final IdentificationType identificationType = IdentificationType.valueOf(userDto.getPerson().getIdentification().getIdentificationType().toString());
        Identification identification = Optional.ofNullable(person.getIdentification()).orElse(Identification.builder().build());
        identification.setIdentificationType(identificationType);
        identification.setNumber(userDto.getPerson().getIdentification().getNumber());

        person.setIdentification(identification);
        user.setPerson(person);

        final RoadType roadType = RoadType.valueOf(userDto.getAddress().getRoadType().toString());

        Address address = Optional.ofNullable(user.getAddress()).orElse(Address.builder().build());
        address.setRoadType(roadType);
        address.setRest(userDto.getAddress().getRest());
        address.setRoad(userDto.getAddress().getRoad());
        address.setNumber(userDto.getAddress().getNumber());
        address.setCountry(userDto.getAddress().getCountry());
        address.setCountry(userDto.getAddress().getCountry());
        address.setProvince(userDto.getAddress().getProvince());
        address.setPostalCode(userDto.getAddress().getPostalCode());
        address.setLocalitation(userDto.getAddress().getLocalitation());

        user.setAddress(address);

        user.setResource(userImg);
        user.setActive(userDto.getActive());

        final String pwd = Optional.ofNullable(user.getPassword()).map(StringUtils::stripToNull).orElse(null);
        final String newPwd = Optional.ofNullable(userDto.getNewPassword()).map(StringUtils::stripToNull).orElse(null);
        if(Objects.isNull(pwd) ||
                (Objects.nonNull(newPwd) &&  !Objects.equals(pwd,newPwd))){
            user.setPassword(userDto.getNewPassword());
        }


        return  userDao.save(user);
    }


    /**
     * This method clean all field of string type that contains an instance {@link UserDto},
     * and return a copy of same without spaces begging and end in that fields
     *
     * @param userDto instance with data of user
     * @return copy of information of user received with the string fields without spaces
     */
    private UserDto clearSpacesInFields(@NonNull UserDto userDto){
        final IdentificationDto identificationDto = IdentificationDto.builder()
                .id( userDto.getPerson().getIdentification().getId())
                .number( StringUtil.clearSpaceFromBoth( userDto.getPerson().getIdentification().getNumber() ))
                .identificationType(userDto.getPerson().getIdentification().getIdentificationType())
                .build();

        final PersonDto personDto = PersonDto.builder()
                .id( userDto.getPerson().getId() )
                .identification(identificationDto)
                .email( StringUtil.clearSpaceFromBoth( userDto.getPerson().getEmail() ) )
                .userName( StringUtil.clearSpaceFromBoth( userDto.getPerson().getUserName() ) )
                .surnames( StringUtil.clearSpaceFromBoth( userDto.getPerson().getSurnames() ) )
                .build();

        final AddressDto addressDto = AddressDto.builder()
                .id(userDto.getAddress().getId() )
                .number( userDto.getAddress().getNumber() )
                .roadType( userDto.getAddress().getRoadType() )
                .postalCode( userDto.getAddress().getPostalCode() )
                .road( StringUtil.clearSpaceFromBoth( userDto.getAddress().getRoad() ))
                .rest( StringUtil.clearSpaceFromBoth( userDto.getAddress().getRest() ))
                .country( StringUtil.clearSpaceFromBoth( userDto.getAddress().getCountry() ))
                .province( StringUtil.clearSpaceFromBoth( userDto.getAddress().getCountry() ))
                .localitation( StringUtil.clearSpaceFromBoth( userDto.getAddress().getLocalitation() ))
                .build();

        return UserDto.builder()
                .person(personDto)
                .address(addressDto)
                .id(userDto.getId())
                .resource(userDto.getResource())
                .password( StringUtil.clearSpaceFromBoth( userDto.getPassword() ) )
                .newPassword( StringUtil.clearSpaceFromBoth( userDto.getNewPassword() ))
                .build();
    }

}
