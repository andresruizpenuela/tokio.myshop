package com.tokioschool.myshop.service.product.impl;

import com.tokioschool.myshop.domain.product.Product;
import com.tokioschool.myshop.domain.resource.Resource;
import com.tokioschool.myshop.dto.product.ProductDto;
import com.tokioschool.myshop.dto.resource.ResourceDto;
import com.tokioschool.myshop.repository.product.ProductDao;
import com.tokioschool.myshop.service.product.IProductService;
import com.tokioschool.myshop.service.resource.ResourceShopService;
import jakarta.annotation.Nullable;
import jakarta.transaction.Transactional;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements IProductService {

    private final ProductDao productDao;
    private final ResourceShopService resourceShopService;

    private final ModelMapper modelMapper;

    @Override
    public Page<ProductDto> findProductPaginateByName(@Nullable String keyword, @NonNull Integer page, @NonNull Integer pageSize) {
        final String filterByName = Optional.ofNullable(keyword)
                .map(StringUtils::stripToEmpty)
                .orElse(StringUtils.EMPTY);

        final int numberProductsAvailable;
        if(StringUtils.isEmpty(filterByName)){
            numberProductsAvailable = this.getNumberProductsAvailable();
        }else{
            numberProductsAvailable =  this.getNumberProductsByNameAvailable(filterByName);
        }

        if(numberProductsAvailable == 0){
            return Page.empty();
        }

        final int numberPageRequest = validatePage(page,pageSize,numberProductsAvailable);
        final int numberElementsForPage = validatePageSize(pageSize,numberProductsAvailable);

        final PageRequest pageRequest = PageRequest.of(numberPageRequest,numberElementsForPage, Sort.by(Sort.Direction.DESC,"name"));

        final Page<Product> productsPage;
        if(StringUtils.isEmpty(filterByName)){
            //productsPage = productDao.findAllByVisiblyIsTrue(pageRequest);
            productsPage = productDao.findAll(pageRequest);

        }else{
            productsPage = productDao.findAllByNameContainingIgnoreCaseAndVisiblyIsTrue(filterByName,pageRequest);

        }


        final List<ProductDto> productDtoList = productsPage.getContent()
                .stream()
                .map(product -> modelMapper.map(product, ProductDto.class)).toList();

        return  new PageImpl<>(productDtoList,pageRequest,numberProductsAvailable);
    }
    public List<Product> findProductByName(@NonNull String filterName){
        final UnaryOperator<String> cleanSpaces = StringUtils::stripToNull;
        final Predicate<String> isStrNull = Objects::isNull;

        if(isStrNull.test(cleanSpaces.apply(filterName))){
            return this.productDao.findProductByVisiblyIsTrue();
        }else{
            return this.productDao.findProductByNameContainsIgnoreCaseAndVisiblyIsTrue(filterName);
        }
    }

    @Override
    public ProductDto getProductById(@NonNull Long id) throws IllegalArgumentException {

        final Product product = productDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Product not found!"));

        return modelMapper.map(product,ProductDto.class);
    }

    @Override
    public ProductDto editProduct(@NonNull Long productId, @NonNull ProductDto productDto, @Nullable MultipartFile multipartFile) {
        Product product = productDao.findById(productId)
                .orElseThrow(()-> new IllegalArgumentException("Product with id %d not found!".formatted(productId)));

        product = updateProduct(product,productDto,multipartFile);

        return modelMapper.map(product,ProductDto.class);
    }

    @Override
    public ProductDto createProduct(@NonNull ProductDto productDto, @Nullable MultipartFile multipartFile) {

        Product product = Product.builder().build();
        product = updateProduct(product,productDto,multipartFile);

        return modelMapper.map(product,ProductDto.class);
    }

    /**
     *
     * @param productId
     */
    @Override
    public void deleteProductById(@NonNull Long productId) {
        this.productDao.deleteById(productId);
    }

    /**
     * @param productId
     */
    @Override
    @Transactional
    public void updateVisiblyProduct(@NonNull Long productId) {
        final boolean visibly = this.productDao.findById(productId).map(Product::getVisibly)
                .orElseThrow(() -> new IllegalArgumentException("Product not found!"));
        this.productDao.updateVisiblyProduct(productId,!visibly);
    }

    private Product updateProduct(Product product, ProductDto productDto, MultipartFile multipartFile) {

        Resource resource = Optional.ofNullable(product.getImage())
                .orElse(null);
        if(Objects.nonNull(multipartFile) && !multipartFile.isEmpty()){
            final ResourceDto resourceDto = resourceShopService.saveResource(multipartFile);
            resource = modelMapper.map(resourceDto,Resource.class);
        }

        product.setImage(resource);
        product.setName(StringUtils.stripToNull(productDto.getName()));
        product.setStock(productDto.getStock());
        product.setPrice(productDto.getPrice());
        product.setTaxes(productDto.getTaxes());
        product.setVisibly(productDto.getVisibly());
        product.setDiscount(productDto.getDiscount());
        product.setCategory(productDto.getCategory());
        product.setDescription(productDto.getDescription());

        return productDao.save(product);
    }


    private int getNumberProductsAvailable(){
        return productDao.getSizeProductsVisibly();
    }
    private int getNumberProductsByNameAvailable(@NonNull String name){
        if(StringUtils.EMPTY.equals(name)){
            return getNumberProductsAvailable();
        }
        return productDao.getSizeProductsVisiblyByNameContains(name);
    }

    private int validatePageSize(int productsForPage,int totalProducts) {
        if( productsForPage<=0 || productsForPage >= totalProducts){
            return totalProducts;
        }
        return productsForPage;
    }

    private int numberPageAllow(int elementsForPage, int totalElements){
        return (int) Math.ceil(totalElements/(double)elementsForPage);
    }

    private int validatePage(int pageRequest, int productsForPage, int totalProducts) {
        // Page go 0 to ( Match.cell(totalProducts/(double)productsForPage) -1 )
        final int numberPageAllow = this.numberPageAllow(productsForPage,totalProducts);
        if(pageRequest <= BigInteger.ZERO.intValue() ){
            // return last page
            return numberPageAllow - BigInteger.ONE.intValue();
        }
        if(pageRequest > numberPageAllow){
            // return first page
            return BigInteger.ZERO.intValue();
        }
        // return page request
        return  pageRequest - BigInteger.ONE.intValue();
    }

}
