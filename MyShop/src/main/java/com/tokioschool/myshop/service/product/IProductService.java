package com.tokioschool.myshop.service.product;

import com.tokioschool.myshop.domain.product.Product;
import com.tokioschool.myshop.dto.product.ProductDto;
import jakarta.annotation.Nullable;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IProductService {

    Page<ProductDto> findProductPaginateByName(@Nullable String keyword, @NonNull Integer page, @NonNull Integer pageSize);
    List<Product> findProductByName(@NonNull String filterName);

    ProductDto getProductById(@NonNull Long id) throws IllegalArgumentException;

    ProductDto editProduct(@NonNull Long productId, @NonNull ProductDto productDto,@Nullable MultipartFile multipartFile);

    ProductDto createProduct(@NonNull ProductDto productDto,@Nullable  MultipartFile multipartFile);

    void deleteProductById(@NonNull Long productId);

    void updateVisiblyProduct(@NonNull Long productId);
}
