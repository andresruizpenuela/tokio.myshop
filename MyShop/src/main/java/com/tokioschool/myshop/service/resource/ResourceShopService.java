package com.tokioschool.myshop.service.resource;

import com.tokioschool.myshop.dto.resource.ResourceDto;
import lombok.NonNull;
import org.springframework.web.multipart.MultipartFile;

public interface ResourceShopService {

    ResourceDto saveResource(@NonNull MultipartFile multipartFile);
}
