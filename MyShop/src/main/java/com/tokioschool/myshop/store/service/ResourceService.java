package com.tokioschool.myshop.store.service;

import com.tokioschool.myshop.store.dto.ResourceContentDto;
import com.tokioschool.myshop.store.dto.ResourceIdDto;
import jakarta.annotation.Nullable;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

public interface ResourceService {
    Optional<ResourceIdDto> saveResource(MultipartFile multipartFile, @Nullable String description);

    Optional<ResourceContentDto> findResource(UUID resourceId);

    void deleteResource(UUID resourceId);

}
