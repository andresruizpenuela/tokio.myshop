package com.tokioschool.myshop.store.util;

import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
public class LogUtil {

    private LogUtil(){}

    public static void showErrorMessage(Exception ex) {
        final StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));

        final String stacktrace = sw.toString();
        final String stringBuilder = String.valueOf("%s : %s : %s \n%s");
        final String message = stringBuilder.formatted(ex.getClass(), ex.getMessage(), ex.getCause(),stacktrace);

        log.error(message);
    }
}
