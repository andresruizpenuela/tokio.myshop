package com.tokioschool.myshop.store.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ResourceConfigProperty.class)
public class ResourceConfig {

}
