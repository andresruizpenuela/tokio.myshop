package com.tokioschool.myshop.store.util;

import com.tokioschool.myshop.store.dto.ResourceContentDto;

import java.util.Objects;
import java.util.function.Predicate;

public class ResourceUtil {

    public static final Predicate<ResourceContentDto> isResourceContentValidate =
            resourceContentDto -> Objects.nonNull(resourceContentDto.contentType())
                && resourceContentDto.size() > 0
                && resourceContentDto.content().length > 0
                && Objects.nonNull(resourceContentDto.resourceId())
                && Objects.nonNull(resourceContentDto.resourceName());

}
