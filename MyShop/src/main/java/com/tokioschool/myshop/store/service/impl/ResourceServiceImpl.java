package com.tokioschool.myshop.store.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tokioschool.myshop.store.config.ResourceConfigProperty;
import com.tokioschool.myshop.store.domain.ResourceDescription;
import com.tokioschool.myshop.store.dto.ResourceContentDto;
import com.tokioschool.myshop.store.dto.ResourceIdDto;
import com.tokioschool.myshop.store.service.ResourceService;
import com.tokioschool.myshop.store.util.FileUtil;
import com.tokioschool.myshop.store.util.LogUtil;
import com.tokioschool.myshop.store.util.ResourceUtil;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ResourceServiceImpl implements ResourceService {

    /* DEPENDENCIES */
    private final ResourceConfigProperty resourceConfigProperty;
    private final ObjectMapper objectMapper; // from package com.fasterxml.jackson.databind

    /* CONSTANTS */
    public static final String NAME_RESOURCE_JSON = "%s.json";




    @Override
    public Optional<ResourceIdDto> saveResource(MultipartFile multipartFile, @Nullable String description) {
        final ResourceIdDto resourceIdDto;
        final ResourceDescription resourceDescription;

        // paths of resources to save
        Path pathResourceContent = null;
        Path pathResourceDescriptionFile = null;

        try {
            //
            resourceIdDto = ResourceIdDto
                    .builder()
                    .resourceId(UUID.randomUUID())
                    .build();
            resourceDescription  = generateResourceDescription(multipartFile,description);

            // path resource content and description
            pathResourceContent = resourceConfigProperty
                    .getdResourcePathFromRelativePathGivenNameResource(resourceIdDto.resourceId().toString());

            pathResourceDescriptionFile = resourceConfigProperty
                    .getdResourcePathFromRelativePathGivenNameResource(NAME_RESOURCE_JSON.formatted(resourceIdDto.resourceId().toString()));

            // save (write) of the resources
            Files.write(pathResourceContent, multipartFile.getBytes());
            this.objectMapper.writeValue(pathResourceDescriptionFile.toFile(),resourceDescription);

        } catch (IllegalArgumentException | IOException e) {
            LogUtil.showErrorMessage(e);
            try {
                FileUtil.deleteWorkIfNotExists(pathResourceContent);
                FileUtil.deleteWorkIfNotExists(pathResourceDescriptionFile);
            } catch (IOException ex) {
                LogUtil.showErrorMessage(ex);
            }
            return Optional.empty();
        }

        return Optional.of(resourceIdDto);
    }

    @Override
    public Optional<ResourceContentDto> findResource(UUID resourceId) {
        final FilenameFilter filenameFilter = (dir,name) -> name.contains("%s".formatted(resourceId));
        final Optional<File> optionalFileDescription;
        final Optional<File> optionalFileContent;

        try {
            final File[] findFiles = this.resourceConfigProperty.buildResourcePathFromRelativePathGivenNameResource()
                    .toFile().listFiles(filenameFilter);

            // section for read resource description
            optionalFileDescription = Arrays.stream(findFiles)
                    .filter(file -> file.getName().equals(NAME_RESOURCE_JSON.formatted(resourceId.toString())))
                    .findFirst();

            if(optionalFileDescription.isEmpty()){
                return Optional.empty();
            }
            final File fileResourceDescription = optionalFileDescription.get();
            final ResourceDescription resourceDescription = this.objectMapper.readValue(fileResourceDescription,
                    ResourceDescription.class);

            // section for read resource content
            optionalFileContent = Arrays.stream(findFiles)
                    .filter(file -> !file.getName().equals(NAME_RESOURCE_JSON.formatted(resourceId.toString())))
                    .findFirst();

            if(optionalFileContent.isEmpty()){
                return Optional.empty();
            }
            final File fileContent = optionalFileContent.get();
            final byte[] bytes = Files.readAllBytes(fileContent.toPath());

            // build the result end, after read resources
            final ResourceContentDto resourceContentDto = ResourceContentDto.builder()
                    .contentType(resourceDescription.getContentType())
                    .description(resourceDescription.getDescription())
                    .resourceName(resourceDescription.getResourceName())
                    .size(resourceDescription.getSize())
                    .content(bytes)
                    .resourceId(resourceId)
                    .build();

            if(!ResourceUtil.isResourceContentValidate.test(resourceContentDto)){
                return Optional.empty();
            }

            return  Optional.of(resourceContentDto);

        }catch (SecurityException | NullPointerException | IOException ex){
            LogUtil.showErrorMessage(ex);
            return Optional.empty();
        }
    }

    @Override
    public void deleteResource(UUID resourceId) {
        final FilenameFilter filenameFilter = (dir, name) -> name.contains("%s".formatted(NAME_RESOURCE_JSON.formatted(resourceId)));

        final File[] findFiles = this.resourceConfigProperty.buildResourcePathFromAbsolutePath()
                .toFile().listFiles(filenameFilter);

        try {
            Arrays.stream(findFiles).forEach(File::delete);
        }catch (SecurityException  ex){
            LogUtil.showErrorMessage(ex);
        }



    }

    private ResourceDescription generateResourceDescription(@Nonnull MultipartFile multipartFile,@Nullable  String description) throws IllegalArgumentException {
        boolean isMultipartFileValid = validateMultipartFile(multipartFile);
        if(Boolean.FALSE.equals(isMultipartFileValid)){
            throw new IllegalArgumentException("Multipart isn't validate");
        }
        return  ResourceDescription.builder()
                .resourceName(multipartFile.getOriginalFilename())
                .contentType(multipartFile.getContentType())
                .size( (int) multipartFile.getSize())
                .description(description)
                .build();
    }


    private boolean validateMultipartFile(@Nonnull MultipartFile multipartFile) {
        return Objects.nonNull(multipartFile.getOriginalFilename())
                && Objects.nonNull(multipartFile.getContentType());
    }

}
