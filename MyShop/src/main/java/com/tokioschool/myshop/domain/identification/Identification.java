package com.tokioschool.myshop.domain.identification;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="IDENTIFICATIONS")
public class Identification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="NUMBER_ID",unique = true,nullable = false)
    private String number;

    @Enumerated(EnumType.STRING)
    @JoinColumn(name="IDENTIFICATION_TYPE",nullable = false )
    private IdentificationType identificationType;

}
