package com.tokioschool.myshop.domain.identification;

public enum IdentificationType {
    NIF,CIF,PASSPORT,SID,OTRO
}
