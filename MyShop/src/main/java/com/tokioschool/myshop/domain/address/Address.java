package com.tokioschool.myshop.domain.address;

import com.tokioschool.myshop.domain.invoice.Invoice;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="addresses")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "road_type",nullable = false)
    private RoadType roadType;
    @Column(nullable = false)
    private String road;
    private Integer number;
    private String rest;
    @Column(name = "postal_code",nullable = false)
    private Integer postalCode;
    @Column(nullable = false)
    private String localitation;
    @Column(nullable = false)
    private String province;
    @Column(nullable = false)
    private String country;

    @OneToMany(mappedBy = "address",cascade = CascadeType.PERSIST,orphanRemoval = true)
    private List<Invoice> invoices;

}
