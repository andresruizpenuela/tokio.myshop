package com.tokioschool.myshop.domain.person;

import com.tokioschool.myshop.domain.identification.Identification;
import com.tokioschool.myshop.domain.invoice.Invoice;
import com.tokioschool.myshop.domain.resource.Resource;
import com.tokioschool.myshop.util.genearte.tsid.TSIdGenerator;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@Entity
@Table(name="PERSONS")
@AllArgsConstructor @NoArgsConstructor
public class Person {
    @Id
    @TSIdGenerator
    private String id;

    @Column(name="user_name")
    private String userName;
    private String surnames;
    @Column(nullable = false,unique = true)
    private String email;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "identification",nullable = false,unique = true)
    private Identification identification;

    @OneToMany(mappedBy = "person",cascade = CascadeType.PERSIST,orphanRemoval = true)
    private List<Invoice> invoices;

}
