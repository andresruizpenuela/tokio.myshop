package com.tokioschool.myshop.domain.product;

import com.tokioschool.myshop.domain.order.OrderDetail;
import com.tokioschool.myshop.domain.resource.Resource;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    @Column(length = 25)
    private String category;
    @JoinColumn(columnDefinition = "DEFAULT(TRUE)")
    private Boolean visibly;
    private BigDecimal stock;
    private BigDecimal price;
    private BigDecimal discount;
    private BigDecimal taxes;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "image")
    private Resource image;


    @OneToMany(mappedBy = "product",orphanRemoval = true,cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
    private List<OrderDetail> orderDetail;

}
