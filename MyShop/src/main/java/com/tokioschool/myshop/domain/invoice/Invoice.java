package com.tokioschool.myshop.domain.invoice;

import com.tokioschool.myshop.domain.address.Address;
import com.tokioschool.myshop.domain.person.Person;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CurrentTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="INVOICES")
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true,nullable = false)
    private String number;

    @ManyToOne
    @JoinColumn(name = "person",nullable = false)
    private Person person;

    @ManyToOne
    @JoinColumn(name = "address",nullable = false)
    private Address address;

    @Column(nullable = false)
    private BigDecimal subtotal;
    @Column(nullable = false)
    private BigDecimal taxes;
    @Column(nullable = false)
    private BigDecimal total;

    @CurrentTimestamp
    @JoinColumn(name = "create_date")
    private LocalDateTime createDate;

    @Column(name="due_date")
    private LocalDateTime dueDate;
}
