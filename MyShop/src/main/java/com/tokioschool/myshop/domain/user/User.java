package com.tokioschool.myshop.domain.user;

import com.tokioschool.myshop.domain.address.Address;
import com.tokioschool.myshop.domain.order.OrderDetail;
import com.tokioschool.myshop.domain.person.Person;
import com.tokioschool.myshop.domain.resource.Resource;
import com.tokioschool.myshop.util.genearte.tsid.TSIdGenerator;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="USERS")
public class User {

    @Id
    @TSIdGenerator
    private String id;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="person",nullable = false,unique = true)
    private Person person;

    @Column(nullable = false)
    private String password;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name="address",nullable = false)
    private Address address;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name="image")
    private Resource resource;

    @JoinColumn(columnDefinition = "DEFAULT(1)")
    private Boolean active;

    @OneToMany(mappedBy = "user")
    private List<OrderDetail> orderDetails;
}
