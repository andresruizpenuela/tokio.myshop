package com.tokioschool.myshop.domain.resource;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="RESOURCES")
public class Resource {

    @Id
    //@UuidGenerator // Management for application
    private UUID id;

    @Column(nullable = false)
    private String name;
    @Column(name = "content_type",nullable = false)
    private String contentType;
    @Column(nullable = false)
    private Integer size;
}
