package com.tokioschool.myshop.util.genearte.tsid;

import com.github.f4b6a3.tsid.TsidCreator;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.factory.spi.CustomIdGeneratorCreationContext;

import java.lang.reflect.Member;

public class TimestampIdGenerator implements IdentifierGenerator {
    public TimestampIdGenerator(TSIdGenerator config, Member annotationMember, CustomIdGeneratorCreationContext context){}
    @Override
    public Object generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o) {
        // usamos la libreria TsIdCreator para generar un id, como formato String
        return TsidCreator.getTsid256().toString();
    }
}