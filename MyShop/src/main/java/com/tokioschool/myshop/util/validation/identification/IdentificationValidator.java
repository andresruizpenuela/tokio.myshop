package com.tokioschool.myshop.util.validation.identification;

import com.tokioschool.myshop.dto.identification.IdentificationDto;
import com.tokioschool.myshop.dto.person.PersonDto;
import com.tokioschool.myshop.util.helper.IdentificationHelper;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class IdentificationValidator implements ConstraintValidator<IdentificationValidation, IdentificationDto> {

    /**
     * @param constraintAnnotation
     */
    @Override
    public void initialize(IdentificationValidation constraintAnnotation) {
    }


    @Override
    public boolean isValid(IdentificationDto identificationDto, ConstraintValidatorContext constraintValidatorContext) {
        return IdentificationHelper.isIdentificationOther(identificationDto) ||
                IdentificationHelper.isIdentificationPassport(identificationDto)
                || IdentificationHelper.isIdentificationDNI(identificationDto)
                || IdentificationHelper.isIdentificationCIF(identificationDto)
                || IdentificationHelper.isIdentificationSID(identificationDto);
    }
}
