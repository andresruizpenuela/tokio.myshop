package com.tokioschool.myshop.util.helper;

import org.apache.commons.lang3.StringUtils;

import java.util.function.UnaryOperator;

public class StringUtil {

    private StringUtil(){
        // delete operation new
    }

    private static final UnaryOperator<String> clearSpaceFromBoth = StringUtils::stripToEmpty;

    /**
     * Function that remove the space from both, beggeing and end of string and
     * return black if the input param is only spaces or null
     */
    public static String clearSpaceFromBoth(String field){
        return StringUtil.clearSpaceFromBoth.apply(field);
    }

}
