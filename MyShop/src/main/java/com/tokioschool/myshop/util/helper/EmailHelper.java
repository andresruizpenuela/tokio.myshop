package com.tokioschool.myshop.util.helper;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class EmailHelper {
    public static final String regex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
    private static final Pattern patternEmail = Pattern.compile(EmailHelper.regex);

    public static final Predicate<String> isEmailValid = email -> patternEmail.matcher(email).matches();



}
