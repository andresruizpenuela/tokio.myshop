package com.tokioschool.myshop.util.validation.identification;

import com.tokioschool.myshop.dto.person.PersonDto;
import com.tokioschool.myshop.util.helper.IdentificationHelper;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PersonIdentificationValidator implements ConstraintValidator<PersonIdentificationValidation, PersonDto> {

    /**
     * @param constraintAnnotation
     */
    @Override
    public void initialize(PersonIdentificationValidation constraintAnnotation) {
    }


    @Override
    public boolean isValid(PersonDto personDto, ConstraintValidatorContext constraintValidatorContext) {
        return IdentificationHelper.isIdentificationDNI(personDto.getIdentification());
    }
}
