package com.tokioschool.myshop.util.validation.password;

import com.tokioschool.myshop.dto.user.UserDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<PasswordValidation, UserDto> {

    private static final Pattern patternPWD = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{8,20}$");

    /**
     * @param constraintAnnotation
     */
    @Override
    public void initialize(PasswordValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    /**
     * @param userDto
     * @param constraintValidatorContext
     * @return
     */
    @Override
    public boolean isValid(UserDto userDto, ConstraintValidatorContext constraintValidatorContext) {
        final String pwd = Optional.ofNullable(userDto.getPassword())
                .map(StringUtils::stripToNull)
                .orElse(null);
        final String newPwd = Optional.ofNullable(userDto.getNewPassword())
                .map(StringUtils::stripToNull)
                .orElse(null);

        return  (Objects.nonNull(pwd) && Objects.nonNull(newPwd)) ||
                (Objects.nonNull(pwd) && Objects.isNull(newPwd)) ||
                Objects.nonNull(newPwd) && patternPWD.matcher(newPwd).matches() && (
                        (Objects.isNull(pwd) && Objects.nonNull(newPwd) )
                        || (Objects.nonNull(pwd) && Objects.nonNull(newPwd) ) );

    }
}
