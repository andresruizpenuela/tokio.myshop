package com.tokioschool.myshop.util.helper;

import com.tokioschool.myshop.dto.identification.IdentificationDto;
import com.tokioschool.myshop.dto.identification.IdentificationTypeDto;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class IdentificationHelper {
    private static final Pattern patternDNI = Pattern.compile("[0-9]{7,8}[TRWAGMYFPDXBNJZSQVHLCKE]");

    private static final Pattern patternCIF = Pattern.compile("[A-HJNP-SUW][0-9]{8}");

    private static final Pattern patternPassport = Pattern.compile("[A-Z]{2}[0-9]{7}");

    private static final Pattern patternSID= Pattern.compile("SID_\\w+");

    public static boolean isIdentificationOther(IdentificationDto identificationDto){
        return Objects.nonNull(identificationDto) && Optional.of(identificationDto)
                .filter(containsIdentificationTypeAnIdentificationType.and(containsIdentificationDtoAnIdentificationTypeOfOther)::test)
                .map(IdentificationDto::getNumber)
                .map(StringUtils::stripToNull)
                .isPresent();
    }

    public static boolean isIdentificationDNI(IdentificationDto identificationDto){
        return Objects.nonNull(identificationDto) && Optional.of(identificationDto)
                .filter(containsIdentificationTypeAnIdentificationType.and(containsIdentificationDtoAnIdentificationTypeOfDNI)::test)
                .map(IdentificationDto::getNumber)
                .map(String::toUpperCase)
                .filter(isValidNumberDNI::test)
                .isPresent();
    }

    public static boolean isIdentificationCIF(IdentificationDto identificationDto){
        return Objects.nonNull(identificationDto) && Optional.of(identificationDto)
                .filter(containsIdentificationTypeAnIdentificationType.and(containsIdentificationDtoAnIdentificationTypeOfCIF)::test)
                .map(IdentificationDto::getNumber)
                .map(String::toUpperCase)
                .filter(isValidNumberCIF::test)
                .isPresent();
    }

    public static boolean isIdentificationPassport(IdentificationDto identificationDto){
        return Objects.nonNull(identificationDto) && Optional.of(identificationDto)
                .filter(containsIdentificationTypeAnIdentificationType.and(containsIdentificationDtoAnIdentificationTypeOfPassport)::test)
                .map(IdentificationDto::getNumber)
                .map(String::toUpperCase)
                .filter(isValidNumberPassport::test)
                .isPresent();
    }

    public static boolean isIdentificationSID(IdentificationDto identificationDto){
        return Objects.nonNull(identificationDto) && Optional.of(identificationDto)
                .filter(containsIdentificationTypeAnIdentificationType.and(containsIdentificationDtoAnIdentificationTypeOfSID)::test)
                .map(IdentificationDto::getNumber)
                .map(String::toUpperCase)
                .filter(isValidNumberSID::test)
                .isPresent();
    }

    private static Predicate<IdentificationDto> containsIdentificationTypeAnIdentificationType = identificationDto -> Objects.nonNull(identificationDto.getIdentificationType());

    /** Valid Ohter **/
    private static Predicate<IdentificationDto> containsIdentificationDtoAnIdentificationTypeOfOther = identificationDto -> Objects.nonNull(identificationDto.getIdentificationType())
            && IdentificationTypeDto.OTRO.equals(identificationDto.getIdentificationType());

    /** Valid DNI **/
    private static Predicate<IdentificationDto> containsIdentificationDtoAnIdentificationTypeOfDNI = identificationDto -> Objects.nonNull(identificationDto.getIdentificationType())
            && IdentificationTypeDto.NIF.equals(identificationDto.getIdentificationType());
    private static Predicate<String> isValidNumberDNI = number ->  patternDNI.matcher(number).matches();

    /** Valid CIF **/
    private static Predicate<IdentificationDto> containsIdentificationDtoAnIdentificationTypeOfCIF = identificationDto -> Objects.nonNull(identificationDto.getIdentificationType())
            && IdentificationTypeDto.NIF.equals(identificationDto.getIdentificationType());
    private static Predicate<String> isValidNumberCIF = number ->  patternCIF.matcher(number).matches();

    /** Valid Passport **/
    private static Predicate<IdentificationDto> containsIdentificationDtoAnIdentificationTypeOfPassport = identificationDto -> Objects.nonNull(identificationDto.getIdentificationType())
            && IdentificationTypeDto.PASSPORT.equals(identificationDto.getIdentificationType());
    private static Predicate<String> isValidNumberPassport = number ->  patternPassport.matcher(number).matches();

    /** Valid SID **/
    private static Predicate<IdentificationDto> containsIdentificationDtoAnIdentificationTypeOfSID= identificationDto -> Objects.nonNull(identificationDto.getIdentificationType())
            && IdentificationTypeDto.SID.equals(identificationDto.getIdentificationType());
    private static Predicate<String> isValidNumberSID = number ->  patternSID.matcher(number).matches();

}
