package com.tokioschool.myshop.util.validation.identification;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = IdentificationValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IdentificationValidation {
    String message() default "Invalid identification number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
