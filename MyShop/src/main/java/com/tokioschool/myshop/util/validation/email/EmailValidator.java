package com.tokioschool.myshop.util.validation.email;

import com.tokioschool.myshop.util.helper.EmailHelper;
import com.tokioschool.myshop.util.validation.identification.IdentificationValidation;
import com.tokioschool.myshop.util.validation.identification.PersonIdentificationValidation;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Optional;

public class EmailValidator implements ConstraintValidator<EmailValidation, String> {

    @Override
    public void initialize(EmailValidation constraintAnnotation) {
    }
    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        return Optional.ofNullable(email)
                .map(EmailHelper.isEmailValid::test)
                .orElse(Boolean.FALSE);
    }
}
