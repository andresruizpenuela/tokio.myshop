package com.tokioschool.myshop.confing;

import com.tokioschool.myshop.domain.address.Address;
import com.tokioschool.myshop.domain.identification.Identification;
import com.tokioschool.myshop.domain.identification.IdentificationType;
import com.tokioschool.myshop.domain.person.Person;
import com.tokioschool.myshop.domain.resource.Resource;
import com.tokioschool.myshop.dto.addres.AddressDto;
import com.tokioschool.myshop.dto.identification.IdentificationDto;
import com.tokioschool.myshop.dto.identification.IdentificationTypeDto;
import com.tokioschool.myshop.dto.person.PersonDto;
import com.tokioschool.myshop.dto.resource.ResourceDto;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserMapperConfig {

    private final ModelMapper modelMapper;

    public UserMapperConfig(ModelMapper modelMapper){
        this.modelMapper = modelMapper;
        configMapperResource();

    }

    public void configMapperResource(){
        this.modelMapper.typeMap(Resource.class, ResourceDto.class);
        this.modelMapper.typeMap(Person.class, PersonDto.class);
        this.modelMapper.typeMap(Identification.class, IdentificationDto.class);
        this.modelMapper.typeMap(Address.class, AddressDto.class);
    }
}
