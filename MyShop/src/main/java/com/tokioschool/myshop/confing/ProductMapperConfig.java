package com.tokioschool.myshop.confing;

import com.tokioschool.myshop.domain.resource.Resource;
import com.tokioschool.myshop.dto.resource.ResourceDto;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductMapperConfig {

    private final ModelMapper modelMapper;

    public ProductMapperConfig(ModelMapper modelMapper){
        this.modelMapper = modelMapper;
        configMapperResource();

    }

    public void configMapperResource(){
        this.modelMapper.typeMap(Resource.class, ResourceDto.class);
    }
}
