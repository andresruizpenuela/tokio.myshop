package com.tokioschool.myshop.proyection.product;

public interface ProductCounterByCategory {

    String getCategory();
    Integer getCounter();

}
