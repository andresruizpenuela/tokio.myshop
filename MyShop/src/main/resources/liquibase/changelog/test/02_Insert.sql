--liquibase formatted sql
--changeset arp:insert-data-init splitStatements:true endDelimiter:;

INSERT INTO ADDRESSES (ID, ROAD_TYPE,ROAD,`NUMBER`,REST,POSTAL_CODE,LOCALITATION,PROVINCE,COUNTRY) VALUES
    (1,'CALLE','SAN RAMÓN', 3,'1',23400,'UBEDA','JAEN','ESPAÑA');
INSERT INTO ADDRESSES (ID, ROAD_TYPE,ROAD,`NUMBER`,REST,POSTAL_CODE,LOCALITATION,PROVINCE,COUNTRY) VALUES
    (2,'CALLE','SAN RAMÓN', 4,'1',23400,'UBEDA','JAEN','ESPAÑA');

INSERT INTO PRODUCTS (ID,NAME,DESCRIPTION,CATEGORY,VISIBLY,STOCK,PRICE,DISCOUNT,TAXES) VALUES
    (1,'MSI 2015','ORDENADOR DE SOBREMESA DE 2015, CON DDR4, USB3.0, COOLER BOOST 4','ELECTRONICA',FALSE,5,855.5,0,6);
INSERT INTO PRODUCTS (ID,NAME,DESCRIPTION,CATEGORY,STOCK,PRICE,DISCOUNT,TAXES) VALUES
    (2,'MSI 2018','ORDENADOR DE SOBREMESA DE 2015, CON DDR4, USB3.0, COOLER BOOST 4','ELECTRONICA',5,855.5,0,6);
INSERT INTO PRODUCTS (ID,NAME,DESCRIPTION,CATEGORY,STOCK,PRICE,DISCOUNT,TAXES) VALUES
    (3,'MSI 2019','ORDENADOR DE SOBREMESA DE 2015, CON DDR4, USB3.0, COOLER BOOST 4','ELECTRONICA',5,855.5,0,6);
INSERT INTO PRODUCTS (ID,NAME,DESCRIPTION,CATEGORY,STOCK,PRICE,DISCOUNT,TAXES) VALUES
    (4,'MSI 2013','ORDENADOR DE SOBREMESA DE 2015, CON DDR4, USB3.0, COOLER BOOST 4','ELECTRONICA',5,855.5,0,6);

INSERT IGNORE  INTO IDENTIFICATIONS (ID,number_id,identification_type)
    VALUES (1,'0000001','NIF'),(2,'0000002','NIF'),(3,'00000013','NIF');

INSERT INTO PERSONS (ID,USER_NAME,SURNAMES,IDENTIFICATION,EMAIL) VALUES
                                                                     (1,'Andres','Ruiz Peñuela',1,'anres.rpenuela@mail.com'),
                                                                     (2,'Marta','Ruiz Peñuela',2,'marta.rpenuela@mail.com'),
                                                                     (3,'Pedro','Ruiz Peñuela',3,'andres.rpenuela@mail.com');

INSERT INTO USERS (ID,PERSON,PASSWORD,ADDRESS,IMAGE,ACTIVE) VALUES
                                                                (1,1,'aaaaa',1,null,TRUE),
                                                                (2,2,'bbb',1,null,TRUE);


INSERT INTO ORDERS (ID,`NUMBER`,SUBTOTAL,PRICE) VALUES
    (1,'A',2555,0);

INSERT INTO ORDER_DETAILS (ID,PRODUCT,QUANTITY,PRICE,`ORDER`,`USER`) VALUES
                                                                         (1,1,1,855,1,1),
                                                                         (2,2,1,1500,1,1);

INSERT INTO INVOICES (ID,`NUMBER`,PERSON,ADDRESS,SUBTOTAL,TAXES,TOTAL,DUE_DATE) VALUES
    (1,1,1,1,2555,0,2555,NOW());

