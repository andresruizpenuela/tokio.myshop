--liquibase formatted sql
--changeset arp:create-tables splitStatements:true endDelimiter:;



CREATE TABLE IF NOT EXISTS addresses(
                                        id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                        road_type enum('Autopista','Autovía','Avenida','Bulevar','Calle','Calle_peatonal','Callejón','Camino','Cañada_real','Carretera','Carretera_de_circunvalación','Carril','Ciclovía','Corredera','Costanilla','Parque','Pasadizo_elevado','Pasaje','Paseo_marítimo','Plaza','Pretil','Puente','Ronda','Sendero','Travesía','Túnel','Vía_pecuaria','Vía_rápida','Vía_verde','Urbanización') NOT NULL,
    road VARCHAR(255) NOT NULL,
    `number` INT,
    rest VARCHAR(100),
    postal_code INT NOT NULL,
    localitation VARCHAR(60) NOT NULL,
    province VARCHAR(80) NOT NULL,
    country VARCHAR(80) NOT NULL
    );

CREATE TABLE IF NOT EXISTS resources(
                                        id UUID,
                                        `name` VARCHAR(255) NOT NULL,
    content_type VARCHAR(100) NOT NULL,
    `size` INT NOT NULL,
    CONSTRAINT `PK_RESOURCES` PRIMARY KEY (ID)
    );

CREATE TABLE IF NOT EXISTS products(
                                       id BIGINT AUTO_INCREMENT,
                                       `name` VARCHAR(255) NOT NULL,
    description TEXT,
    category VARCHAR(25) NOT NULL,
    visibly BOOLEAN DEFAULT(TRUE),
    stock NUMERIC DEFAULT(0) CHECK  (STOCK >= 0),
    price NUMERIC DEFAULT(0) CHECK  (PRICE >= 0),
    discount NUMERIC DEFAULT(0) COMMENT 'EN TANTO POR CIENTO' CHECK (DISCOUNT >= 0),
    taxes NUMERIC DEFAULT(0) COMMENT 'EN TANTO POR CIENTO' CHECK (DISCOUNT >= 0),
    image UUID,
    CONSTRAINT `PK_PRODCUTS` PRIMARY KEY (ID),
    CONSTRAINT `FK_PRODUCTS_TO_RESOURCES` FOREIGN KEY (IMAGE) REFERENCES RESOURCES(ID)
    );

CREATE TABLE IF NOT EXISTS identifications(
                                              ID BIGINT AUTO_INCREMENT PRIMARY KEY,
                                              number_id VARCHAR(255) NOT NULL UNIQUE COMMENT 'NUMER DE IDENTIFICACION',
    identification_type ENUM('NIF','CIF','PASSPORT','SID') NOT NULL DEFAULT ('PASSPORT')
    );

CREATE TABLE IF NOT EXISTS persons(
    id VARCHAR(20) COMMENT 'GENERATE BY A TEMPORAL MARK',
    user_name VARCHAR(255) NOT NULL COMMENT 'NAME OR COMPANY NAME',
    surnames VARCHAR(255),
    identification BIGINT UNIQUE NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    CONSTRAINT `PK_PERSONS` PRIMARY KEY (ID),
    CONSTRAINT `FK_PERSONS_TO_IDENTIFICATIONS` FOREIGN KEY (identification) REFERENCES identifications(id)
    );

CREATE TABLE IF NOT EXISTS users(
    id VARCHAR(20) COMMENT 'GENERATE BY A TEMPORAL MARK',
    person VARCHAR(20) NOT NULL UNIQUE,
    password TEXT NOT NULL UNIQUE,
    address BIGINT NOT NULL,
    image UUID,
    active BOOLEAN DEFAULT(TRUE),
    CONSTRAINT `PK_USERS` PRIMARY KEY (ID),
    CONSTRAINT `FK_USERS_TO_PERSONS` FOREIGN KEY (person) REFERENCES persons(id)
    );

CREATE TABLE IF NOT EXISTS invoices(
                                       id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                       `number` VARCHAR(255) NOT NULL UNIQUE COMMENT 'INVOICE NUMBER',
    person VARCHAR(20) NOT NULL,
    address BIGINT NOT NULL COMMENT 'SEND ADDRESS',
    subtotal NUMERIC NOT NULL DEFAULT(0) CHECK (SUBTOTAL >= 0),
    taxes NUMERIC NOT NULL DEFAULT(0) CHECK (TAXES >= 0),
    total NUMERIC NOT NULL DEFAULT(0) CHECK (TOTAL >= 0 && SUBTOTAL <= TOTAL),
    create_date TIMESTAMP DEFAULT(now()),
    due_date TIMESTAMP,
    CONSTRAINT `FK_PRODUCTS_TO_PERSON` FOREIGN KEY (person) REFERENCES persons(id),
    CONSTRAINT `FK_INVOICES_TO_ADDRESSES` FOREIGN KEY (address) REFERENCES addresses(id)

    );


CREATE TABLE IF NOT EXISTS orders(
                                     id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                     `number` VARCHAR(255) NOT NULL UNIQUE COMMENT 'ORDER NUMBER',
    subtotal NUMERIC NOT NULL,
    price NUMERIC NOT NULL
    );

CREATE TABLE IF NOT EXISTS order_details(
                                            id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                            product BIGINT,
                                            quantity NUMERIC NOT NULL,
                                            price NUMERIC,
                                            `order` BIGINT,
                                            `user` VARCHAR(20) NOT NULL,
    CONSTRAINT `FK_ORDER_DETAILS_TO_ORDERS` FOREIGN KEY (`order`) REFERENCES orders(id),
    CONSTRAINT `FK_ORDER_DETAILS_TO_USERS` FOREIGN KEY (`user`) REFERENCES users(id)
    );

CREATE TABLE IF NOT EXISTS phones(
                                     id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                     suffix VARCHAR(4) NOT NULL,
    number_phone INT NOT NULL UNIQUE COMMENT 'NUMER DE TELEFONO',
    `user` VARCHAR(20) NOT NULL,
    CONSTRAINT `FK_PHONES_TO_USERS` FOREIGN KEY (`user`) REFERENCES users(id)
    );