--liquibase formatted sql
--changeset arp:drop-colum-user-of-identification splitStatements:true endDelimiter:;

-- la fk de usuarios no es necesaria
ALTER TABLE IDENTIFICATIONS DROP  FOREIGN KEY IF EXISTS FK_IDENTIFICATIONS_TO_USERS;
ALTER TABLE IDENTIFICATIONS DROP  IF EXISTS `USER`;
