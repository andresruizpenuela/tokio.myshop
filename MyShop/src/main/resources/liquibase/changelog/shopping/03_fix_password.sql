--liquibase formatted sql
--changeset arp:fix-pwd splitStatements:true endDelimiter:;
-- the pwd can not be unique
-- SHOW INDEX FROM users;
DROP INDEX IF EXISTS password ON users;
