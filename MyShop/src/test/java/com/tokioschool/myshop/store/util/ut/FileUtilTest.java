package com.tokioschool.myshop.store.util.ut;

import com.tokioschool.myshop.store.util.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


class FileUtilTest {

    @Test
    void givenSegmentEmpty_whenGetCurrentWorking_thenGetCurrentWorkingOk() {
        final Path currentWorking = FileUtil.getCurrentWorking(StringUtils.EMPTY);

        Assertions.assertThat(currentWorking)
                .isNotNull()
                .isDirectory()
                .toString()
                .contains("/MyShop");;
    }

    @Test
    void givenSegmentKnow_whenGetCurrentWorking_thenGetCurrentWithSegment() {
        final Path pathResourceRelative = FileUtil.getCurrentWorking("../resource");

        Assertions.assertThat(pathResourceRelative)
                .isNotNull()
                .matches(path -> !Files.exists(path),"don't exists");
    }

    @Test
    void givenSegmentKnow_whenCreatePath_thenCreatePath() {
        final Path path = FileUtil.getCurrentWorking("../resource");

        final boolean wasCreated;
        try {
            FileUtil.deleteWorkIfNotExists(path);
            wasCreated = FileUtil.createWorkIfNotExists(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Assertions.assertThat(wasCreated)
                .isTrue();
    }
    @Test
    void givenSegmentKnow_whenCreatePath_thenDeletePath() {
        final Path path = FileUtil.getCurrentWorking("../resource");

        final boolean wasDeleted;

        try {
            FileUtil.createWorkIfNotExists(path);
            wasDeleted = FileUtil.deleteWorkIfNotExists(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Assertions.assertThat(wasDeleted)
                .isTrue();
    }
}