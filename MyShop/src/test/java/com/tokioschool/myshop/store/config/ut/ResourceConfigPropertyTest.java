package com.tokioschool.myshop.store.config.ut;

import com.tokioschool.myshop.store.config.ResourceConfigProperty;
import com.tokioschool.myshop.store.util.FileUtil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.nio.file.Path;

@SpringBootTest
@ActiveProfiles("test")
class ResourceConfigPropertyTest {

    @Autowired
    ResourceConfigProperty resourceConfigProperty;

    @Test
    void givenPathRelative_whenGetResourcePathFromRelativePath_returnOk() throws IOException {
        final Path path = resourceConfigProperty.buildResourcePathFromRelativePathGivenNameResource();

        FileUtil.createWorkIfNotExists(path);

        Assertions.assertThat(path)
                .exists()
                .isDirectory();

        FileUtil.deleteWorkIfNotExists(path);
    }

    @Test
    void givenPathRelative_whenGetResourcePathFromAbsolute_returnOk() throws IOException {
        final Path path = resourceConfigProperty.buildResourcePathFromAbsolutePath();

        FileUtil.createWorkIfNotExists(path);

        Assertions.assertThat(path)
                .exists()
                .isDirectory();

        FileUtil.deleteWorkIfNotExists(path);
    }


    @Test
    void givenResourceName_whenGetResourcePathFromRelativePath_returnOk() throws IOException {
        final Path path = resourceConfigProperty.getdResourcePathFromRelativePathGivenNameResource("file.txt");

        FileUtil.createWorkIfNotExists(path);

        Assertions.assertThat(path)
                .exists()
                .isDirectory();

        FileUtil.deleteWorkIfNotExists(path);
    }
}