package com.tokioschool.myshop.mvc.controller.users.ut;

import com.tokioschool.myshop.dto.addres.AddressDto;
import com.tokioschool.myshop.dto.addres.RoadTypeDto;
import com.tokioschool.myshop.dto.identification.IdentificationDto;
import com.tokioschool.myshop.dto.person.PersonDto;
import com.tokioschool.myshop.dto.user.UserDto;
import com.tokioschool.myshop.mvc.controller.UserMvcController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@ExtendWith(SpringExtension.class)
@WebMvcTest( UserMvcController.class)
@ActiveProfiles("test")
class UserMvcControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getFormNewAccount() {
    }

    @Test
    void givenUserURIWithPostAndFormData_whenMockMv_thenVerifyErrorResponse() throws Exception {


        final MockMultipartFile mockMultipartFile
                = new MockMultipartFile(
                "personPhoto",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        final RequestBuilder request = MockMvcRequestBuilders.multipart("/shopping/users/add")
                .file(mockMultipartFile)
                .accept(MediaType.MULTIPART_FORM_DATA_VALUE)
                .flashAttr("user",recoverUserDtoMockWithIdentificationNotValidate()); // read with @ModelAttr

        mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(result -> Assertions.assertThat( result.getModelAndView().getModel().containsKey("org.springframework.validation.BindingResult.user")).isTrue())
                .andExpect(view().name("person/user-detail"))
                .andReturn();

    }

    private static PersonDto recoverPersonDtoMockWithIdentificationEmpty(){
        return PersonDto.builder()
                .userName("andres")
                .surnames("ruiz peñuela")
                .identification(IdentificationDto.builder().build())
                .build();
    }
    private static AddressDto recoverAddressDtoMockWithDataFull(){
        return AddressDto.builder()
                .number(1)
                .roadType(RoadTypeDto.CALLE)
                .road("San Ramon")
                .number(3)
                .rest("1")
                .postalCode(23400)
                .localitation("Ubeda")
                .province("Jaen")
                .country("Spain").build();
    }
    private static UserDto recoverUserDtoMockWithIdentificationNotValidate(){
        return  UserDto.builder()
                .person(recoverPersonDtoMockWithIdentificationEmpty())
                .address(recoverAddressDtoMockWithDataFull())
                .password("abc").build();
    }
}