package com.tokioschool.myshop.mvc.controller.resource.ut;

import com.tokioschool.myshop.mvc.controller.ResourceMvcController;
import com.tokioschool.myshop.store.dto.ResourceContentDto;
import com.tokioschool.myshop.store.service.ResourceService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest( ResourceMvcController.class)
class ResourceMvcControllerUt {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ResourceService resourceService;

    @Test
    void givenInvalidateResourceId_whenDownload_thenReturnNotFound() throws Exception {
        Mockito.when(resourceService.findResource(Mockito.any(UUID.class)))
                        .thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/shopping/resources/download/"+UUID.randomUUID())
                        .accept(MediaType.TEXT_PLAIN_VALUE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void givenInvalidateResourceId_whenDownload_thenReturn200() throws Exception {
        Mockito.when(resourceService.findResource(Mockito.any(UUID.class)))
                .thenReturn(recoverResourceContentDtoMock());

      String content =  mockMvc.perform(MockMvcRequestBuilders.get("/shopping/resources/download/"+UUID.randomUUID())
                        .accept(MediaType.TEXT_PLAIN_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Assertions.assertThat(content)
                .isNotNull()
                .isEqualTo("hola");
    }

    private Optional<ResourceContentDto> recoverResourceContentDtoMock(){
        final ResourceContentDto resourceContentDto = ResourceContentDto.builder()
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .content("hola".getBytes()).build();
        return Optional.of(resourceContentDto);
    }
}