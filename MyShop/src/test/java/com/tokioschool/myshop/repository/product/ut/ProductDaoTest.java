package com.tokioschool.myshop.repository.product.ut;

import com.tokioschool.myshop.domain.product.Product;
import com.tokioschool.myshop.proyection.product.ProductCounterByCategory;
import com.tokioschool.myshop.repository.product.ProductDao;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@SpringBootTest
@DataJpaTest // USE A BBDD IN MEM POR DEFECTO
@ActiveProfiles("test")
class ProductDaoTest {

    @Autowired
    private ProductDao productDao;
    @Test
    void givenARepositoryNotEmpty_whenFindProductsByCategoryContains_returnAListProducts() {
        final List<Product> products = productDao.findProductsByCategoryContains("elec");

        Assertions.assertThat(products)
                .isNotEmpty();
    }

    @Test
    void givenARepositoryNotEmpty_whenFindProductsByNameContains_returnAListProducts() {
        final List<Product> products = productDao.findProductsByNameContains("msi");

        Assertions.assertThat(products)
                .isNotEmpty();
    }

    @Test
    void givenARepositoryNotEmpty_whenFindGetProductCountByCategoryKnow_returnAProductsCounter() {
        final Optional<ProductCounterByCategory> optionalProductElec = productDao.getProductCountByCategory("elec");

        Assertions.assertThat(optionalProductElec)
                .get()
                .returns("ELECTRONICA",ProductCounterByCategory::getCategory)
                .returns(4,ProductCounterByCategory::getCounter);
    }

    @Test
    void givenARepositoryNotEmpty_whenFindGetProductCountByCategoryUnkow_returnAProductsCounterWithCategoryToNullAndCounterZero() {
        final Optional<ProductCounterByCategory> optionalProductAbc = productDao.getProductCountByCategory("abc");

        Assertions.assertThat(optionalProductAbc)
                .get()
                .returns(null,ProductCounterByCategory::getCategory)
                .returns(0,ProductCounterByCategory::getCounter);
    }

    @Test
    void givenARepositoryNotEmpty_whenFindGetProductCountByCategoriesKnow_returnAListProductsCounter() {
        final List<ProductCounterByCategory>  productCounterByCategoriesElec = productDao.getProductCountByCategories();

        Assertions.assertThat(productCounterByCategoriesElec)
                .hasSize(1)
                .first()
                .returns("ELECTRONICA",ProductCounterByCategory::getCategory)
                .returns(4,ProductCounterByCategory::getCounter);
    }

    @Test
    void givenARepositoryNotEmpty_whenFindGetProductCountByCategoriesUnKnow_returnAListProductsCounterWitAElementWithCategoryToNullAndCounterZero() {
        final List<ProductCounterByCategory> productCounterByCategoriesElecAbc = productDao.getProductCountByCategories();

        Assertions.assertThat(productCounterByCategoriesElecAbc)
                .hasSize(1)
                .first()
                .returns("ELECTRONICA",ProductCounterByCategory::getCategory)
                .returns(4,ProductCounterByCategory::getCounter);
    }

    @Test
    void giveAProduct_whenUpdateProductStock_thenReturn0k(){
        Product product = productDao.findById(1L).get();
        final BigDecimal stockOld =  product.getStock();


        final int result = productDao.updateProductStock(product.getId(), product.getStock().add(BigDecimal.TEN));

        product = productDao.findById(1L).get();

        Assertions.assertThat(result)
                .isEqualTo(1);

        Assertions.assertThat(product)
                .returns(stockOld.add(BigDecimal.TEN),Product::getStock);

    }

    @Test
    void givenARepositoryNotEmpty_whenFindAll_thenReturnPage(){
        final PageRequest pageRequest =
                PageRequest.of(0,2, Sort.by(Sort.Direction.DESC,"name"));

        final Page<Product> page1 = productDao.findAll(pageRequest);

        Assertions.assertThat(page1.getTotalPages())
                        .isGreaterThanOrEqualTo(2);
        assertEquals(4,page1.getTotalElements());
        assertEquals(2,page1.getNumberOfElements());
        assertEquals("MSI 2019",page1.getContent().get(0).getName());

        final Page<Product> page2 = productDao.findAll(pageRequest);

        Assertions.assertThat(page2.getTotalPages())
                .isGreaterThanOrEqualTo(2);
        assertEquals(4,page2.getTotalElements());
        assertEquals(2,page2.getNumberOfElements());
        assertEquals("MSI 2019",page2.getContent().get(0).getName());
    }


    @Test
    void givenARepositoryNotEmpty_whenGetSizeProductsVisible_thenReturnOk(){
        int visibleProducts = productDao.getSizeProductsVisibly();

        Assertions.assertThat(visibleProducts)
                .isGreaterThanOrEqualTo(3);
    }

    @Test
    void givenARepositoryNotEmpty_whenGetSizeProductsVisibleByNameContains_thenReturnOk(){
        int visibleProducts = productDao.getSizeProductsVisiblyByNameContains("msi");

        Assertions.assertThat(visibleProducts)
                .isGreaterThanOrEqualTo(3);
    }

    @Test
    void givenARepositoryNotEmpty_whenFindProductByNameContainsIgnoreCaseAndVisibleIsTrue_thenReturnOk(){
        final List<Product> products = productDao.findProductByNameContainsIgnoreCaseAndVisiblyIsTrue("msi");

        Assertions.assertThat(products)
                .hasSizeGreaterThanOrEqualTo(3);
    }

    @Test
    void givenARepositoryNotEmpty_whenFindProductByVisibleIsTrue_thenReturnOk(){
        final List<Product> products = productDao.findProductByVisiblyIsTrue();

        Assertions.assertThat(products)
                .hasSizeGreaterThanOrEqualTo(3);
    }

    @Test
    void givenARepositoryNotEmpty_whenUpdateVisiblyProduct_thenReturnOk(){
        final Boolean beforeVisibly = productDao.findById(1L).map(Product::getVisibly).orElse(null);

        productDao.updateVisiblyProduct(1L, !beforeVisibly);

        final Boolean afterVisibly = productDao.findById(1L).map(Product::getVisibly).orElse(null);


        Assertions.assertThat(afterVisibly)
                .isEqualTo(!beforeVisibly);
    }
}